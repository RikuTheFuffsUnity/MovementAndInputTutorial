﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    [SerializeField]
    float speed = 5;

	// Update is called once per frame
	void Update ()
    {
        //se premo il tasto
        //muovi l'oggetto sull'asse corrispondente al tasto

        float horizontalMovement = Input.GetAxis("Horizontal");
        float verticalMovement = Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(0, 0, 0);

        if (horizontalMovement != 0)
        {
            direction.z = horizontalMovement;
        }

        if (verticalMovement != 0)
        {
            direction.y = verticalMovement;
        }
        transform.Translate(direction * (speed * Time.deltaTime));

    }
}
